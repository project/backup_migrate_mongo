<?php


/**
 * @file
 * A destination type for saving locally to the server.
 */

/**
 * A destination type for saving locally to the server.
 *
 * @ingroup backup_migrate_destinations
 */

class backup_migrate_mongo_destination_mongo extends backup_migrate_destination {

  var $supported_ops = array('restore', 'configure', 'source');

  /**
   * Get the form for the settings for the files destination.
   */
  function edit_form() {
    $form = parent::edit_form();
    $form['mongo_path'] = array(
      '#type' => 'textfield',
      '#title' => t('MongoDB bin path'),
      '#default_value' => variable_get('backup_migrate_mongo_path', NULL),
      '#description' => t('The path to the `mongodump` and `mongorestore` executables.'),
      '#required' => TRUE,
    );
    return $form;
  }

  function edit_form_validate($form, &$form_state) {

    $path = $form_state['values']['mongo_path'];

    if (!file_exists($form_state['values']['mongo_path'])) {
      form_set_error('mongo_path', 'Directory doesn\'t exist');
    }
    if (!is_executable("$path/mongodump")) {
      form_set_error('mongo_path', 'Unable to find mongodump');
    }
    if (!is_executable("$path/mongorestore")) {
      form_set_error('mongo_path', 'Unable to find mongorestore');
    }

  }

  function edit_form_submit($form, &$form_state) {
    parent::edit_form_submit($form, $form_state);
    variable_set('backup_migrate_mongo_path', $form_state['values']['mongo_path']);
  }

  /**
   * Return a list of backup filetypes.
   */
  function file_types() {
    return array(
      "tgz" => array(
        "extension" => "tgz",
        "filemime" => "application/x-gzip",
        "backup" => TRUE,
        "restore" => TRUE,
      ),
    );
  }

  /**
   * Declare the current files directory as a backup source..
   */
  function destinations() {

    $connection = $this->get_mongodb_connection();

    $out  = array();
    $out['mongodb'] = backup_migrate_create_destination('mongodb', array('destination_id' => 'mongodb', 'location' => "{$connection['host']}/{$connection['db']}", 'name' => t('Default MongoDB Database')));
    return $out;
  }

  function get_mongodb_connection ($alias = 'default') {

    $connections = variable_get('mongodb_connections', array());
    $connection = isset($connections[$alias]) ? $connections[$alias] : array();
    $connection += array('host' => 'localhost', 'db' => 'drupal', 'connection_options' => array());
    $options = $connection['connection_options'] +  array('connect' => TRUE);
    return $connection;

  }

  function get_temp_dir() {
    // Create a temporary directory for mongodump to write to.
    $temp = drupal_tempnam(file_directory_temp(), 'mongo');
    unlink($temp);
    mkdir($temp);
    return $temp;
  }

  /**
   * Backup from this source.
   */
  function backup_to_file($file, $settings) {

    $connection = $this->get_mongodb_connection();
    $db = $connection['db'];

    $file->push_type('tar');
    $path = drupal_realpath($file->path);

    $temp = $this->get_temp_dir();

    $bin = variable_get('backup_migrate_mongo_path');
    if (empty($bin) || !is_executable("$bin/mongodump")) {
      // @todo use the destination ID
      backup_migrate_backup_fail('Unable to find mongodump. <a href="!url">Configure MongoDB destination</a>.', array('!url' => url('admin/config/system/backup_migrate/destination/list/mongodb')), $settings);
      return FALSE;
    }

    // @todo figure out what the return value would be
    // Dump mongodb
    exec("$bin/mongodump --db $db --out $temp", $output);
    // tarball the directory into a file
    exec("tar cvf $path -C $temp .");

    return $file;

  }

  /**
   * Restore to this source.
   */
  function restore_from_file($file, &$settings) {

    global $user;

    // Backup and Migrate doesn't delete the `file_managed` record for us
    // It doesn't even tell us the correct filename, so go off the user's uid and the time the file was uploaded
    $file_info = $file->file_info;
    $fid = db_query("SELECT fid FROM {file_managed} WHERE uid = :uid AND uri = :uri", array(
      ':uid' => $user->uid,
      ':uri' => "temporary://{$file_info['filename']}",
    ))->fetchField();
    $file_managed = file_load($fid);

    $source = $settings->get_source();

    $connection = $this->get_mongodb_connection();
    $db = $connection['db'];

    $file = $file->pop_type();
    $path = drupal_realpath($file->path);

    $temp = $this->get_temp_dir();

    $bin = variable_get('backup_migrate_mongo_path');
    if (empty($bin) || !is_executable("$bin/mongorestore")) {
      // @todo use the destination ID
      backup_migrate_backup_fail('Unable to find mongorestore. <a href="!url">Configure MongoDB destination</a>.', array('!url' => url('admin/config/system/backup_migrate/destination/list/mongodb')), $settings);

      // Delete the file
      if ($file_managed) {
        file_delete($file_managed);
      }

      return FALSE;
    }

    backup_migrate_filters_invoke_all('pre_restore', $file, $settings);


    // Extract tarball to a directory
    exec("tar xvf $path -C $temp");

    $dir = current(array_filter(scandir($temp), function ($item) use ($temp) {
      return $item != '.' && $item != '..' && is_dir("$temp/$item");
    }));

    // Restore mongodb
    exec("$bin/mongorestore --drop --db $db $temp/$dir");

    $num = 1;

    backup_migrate_filters_invoke_all('post_restore', $file, $settings, $num);

    // Delete the file
    if ($file_managed) {
      file_delete($file_managed);
    }

    return $file;

  }


}