Notes
===

* This module uses the command line tools `mongodump` and `mongorestore` to read and write database dumps.
  The path to the mongo bin directory must be configured in the Default MongoDB Database edit form, and the
  web server must have permission to execute those files.
* To avoid an error when restoring the same file name multiple times, set 'allow_insecure_uploads' to TRUE.